<!DOCTYPE html>
<html>
<head>
    <title>Main Page</title>
</head>
<body>
    <h1>All Visitors</h1>

    <th>
        <?php 
        $ips = DB::table('visitors')->pluck('ip');

        foreach ($ips as $ip){ 
            echo $ip . "<br>";
        }

        $unique = DB::table('visitors')->distinct()->count('ip');

        foreach ($unique as $uip){ 
            echo $uip . "<br>";    
        }
        ?>
    </th>

</body>
</html 